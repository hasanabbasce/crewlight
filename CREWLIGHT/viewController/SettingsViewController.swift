//
//  SettingsViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 22/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController , UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableview: UITableView!
    
        var settingoptions: [String] = ["Un-Register Device", "Report a Problem" ,"Terms & Conditions"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableview.register(UITableViewCell.self, forCellReuseIdentifier: "cellIdentifier")
        // This view controller itself will provide the delegate methods and row data for the table view.
        tableview.delegate = self
        tableview.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.settingoptions.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 3
    }
    
    // Make the background color show through
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellIdentifier", for: indexPath)
        //cell.selectionStyle = .none
       
            cell.accessoryType = .disclosureIndicator
        
        
        cell.textLabel?.text = self.settingoptions[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        cell.textLabel?.textColor = UIColor.white
        cell.detailTextLabel?.textColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("selected  \(settingoptions[indexPath.row])")
        
        if (self.settingoptions[indexPath.row] == "Un-Register Device")
        {
            let askAlert = UIAlertController(title: "Un-Register Device", message: "Are you sure you want to Un-Register this Device ? This will discard all files and request.", preferredStyle: UIAlertControllerStyle.alert)
            
            askAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.unRegisterDevice()
                
            }))
            
            askAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                print("Handle Cancel Logic here")
            }))
            
            present(askAlert, animated: true, completion: nil)
        }
        if (self.settingoptions[indexPath.row] == "Terms & Conditions")
        {
                                if let termsviewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TermsViewController") as? TermsViewController {
                                    if let navigator = navigationController {
            
                                        termsviewcontroller.title = "Terms & Conditions"
                                        termsviewcontroller.navigationItem.backBarButtonItem?.title = ""
                                        navigator.pushViewController(termsviewcontroller, animated: true)
                                    }
                                }
        }
        if (self.settingoptions[indexPath.row] == "Report a Problem")
        {
            let storyBoard = UIStoryboard(name: "Main", bundle: nil)
            let ReportViewController = storyBoard.instantiateViewController(withIdentifier: "ReportViewController") as! ReportViewController
            ReportViewController.title = "Report a Problem"
            self.navigationController?.pushViewController(ReportViewController, animated: true)
        }
        
        
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
