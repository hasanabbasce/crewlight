//
//  ProfileViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 20/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfileViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    @IBOutlet weak var tableview: UITableView!
    
    @IBOutlet weak var profileimage: UIImageView!
    
   
    @IBOutlet weak var leftone: UILabel!
    
    @IBOutlet weak var lefttwo: UILabel!
    
    @IBOutlet weak var leftthree: UILabel!
    
    @IBOutlet weak var leftfour: UILabel!
    
    @IBOutlet weak var rightone: UILabel!
    
    @IBOutlet weak var righttwo: UILabel!
    
    @IBOutlet weak var rightthree: UILabel!
    
    @IBOutlet weak var rightfour: UILabel!
    
    var tableData = [Any]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileimage.tintColor = UIColor.white
        tableview.dataSource = self
        tableview.delegate = self
        tableview.layoutMargins = .zero
        tableview.separatorInset = .zero
        tableview.register(UINib(nibName: "ProfilewidgetTableViewCell", bundle: nil), forCellReuseIdentifier: "ProfilewidgetTableViewCell")
        tableview.register(UINib(nibName: "CrewCertificateTableViewCell", bundle: nil), forCellReuseIdentifier: "CrewCertificateTableViewCell")
        
        let notificationName = Notification.Name("userdataset")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleuserdataset), name: notificationName, object: nil)
        renderprofile()
        
        
        let notificationName2 = Notification.Name("userpicset")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleuserpicset), name: notificationName2, object: nil)
        renderpic()
        
        // Do any additional setup after loading the view.
    }
    
    @objc func handleuserdataset(withNotification notification : NSNotification) {
        renderprofile()
        
    }
    
    @objc func handleuserpicset(withNotification notification : NSNotification) {
        renderpic()
    }
    
    
    func renderpic()
    {
        if let USERPIC = UserDefaults.standard.value(forKey: "USERPIC") as? Data {
            do {
                
                self.profileimage.image = UIImage(data: USERPIC)
            }
            catch let error as NSError {
                
                print("Error in fetching Data from cache")
            }
        }
    }
    
    
    func renderprofile()
    {
        tableData = []
        if let USERDATA = UserDefaults.standard.value(forKey: "USERDATA") as? Data {
            do {
                
                let json =  try JSONSerialization.jsonObject(with: USERDATA, options: []) as? [String:Any]
                
                if let jsonobject = json!["DATA"]! as? [Any]  {
                    
                    print(jsonobject)
                    
                    print("here")
                   self.tableData = []
                   
                    for dic in jsonobject {
                       
                        if let dicobject = dic as? [Any]  {
                             self.tableData.append(dicobject)
                        }
                    }
                    
                    let celldata = tableData[0] as! [Any]
                    
                    for i in 1 ..< 9 {
                        var texttoshow:String = ""
                        var labletoshow:String = ""
                        if let headingobj = celldata[i] as? [String:Any]
                        {
                            if let keytoshow = Array(headingobj.keys)[0] as? String
                            {
                                labletoshow = keytoshow
                                var labletext = keytoshow + " : "
                                if let texttoshow = Array(headingobj.values)[0] as? String
                                {
                                    labletext =  labletext + texttoshow
                                    
                                }
                                texttoshow = labletext
                            }
                        }
                        if(i == 1)
                        {
                            leftone.text = texttoshow
                            leftone.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 2)
                        {
                            lefttwo.text = texttoshow
                            lefttwo.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 3)
                        {
                            leftthree.text = texttoshow
                            leftthree.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 4)
                        {
                            leftfour.text = texttoshow
                            leftfour.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 5)
                        {
                            rightone.text = texttoshow
                            rightone.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 6)
                        {
                            righttwo.text = texttoshow
                            righttwo.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 7)
                        {
                            rightthree.text = texttoshow
                            rightthree.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                        else if(i == 8)
                        {
                            rightfour.text = texttoshow
                            rightfour.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
                        }
                    }
                    
                    
                    
                    
//                    let basicarryobj = tableData[0] as! [Any]
//                    let CREW_IDobj = basicarryobj[1] as? [String:Any]
//                    if let CREW_ID = CREW_IDobj!["CREW_ID"] as? String
//                    {
//                        self.crewidlabel.text = "CREW ID : " + CREW_ID
//                    }
//                    else
//                    {
//                        self.crewidlabel.text = ""
//                    }
//                    let Nameobj = basicarryobj[2] as? [String:Any]
//                    if let Name = Nameobj!["Name"] as? String
//                    {
//                        self.namelable.text = "NAME : " + Name
//                    }
//                    else
//                    {
//                        self.namelable.text = ""
//                    }
//                    let CrewTypeobj = basicarryobj[3] as? [String:Any]
//                    if let CrewType = CrewTypeobj!["CrewType"] as? String
//                    {
//                        self.crewtype.text = "CREW TYPE : " + CrewType
//                    }
//                    else
//                    {
//                        self.crewtype.text = ""
//                    }
                    
                    
                    self.tableview.reloadData()
                }
            }
            catch let error as NSError {
                
                print("Error in fetching Data from cache")
            }
        }
        
        
        self.tableview.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableData.count - 1
    }
    
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rowdatacount = 0
        
       let sectionarryobj = tableData[section + 1] as! [Any]
       let headingobj = sectionarryobj[0] as? [String:Any]
       if let heading = headingobj!["key"] as? String
       {
         if(heading == "EMPLOYMENT" || heading == "PASSPORT / CNIC INFORMATION")
         {
            rowdatacount = 1
         }
        else
         {
            let dataobj = sectionarryobj[1] as? [String:Any]
            if let datacertificates = dataobj!["data"] as? [[String:String]]
            {
                rowdatacount = datacertificates.count + 1
            }
        }
        
        }
        return rowdatacount

    }
    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//
//        return 1
//
//    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let sectionarryobj = tableData[indexPath.section + 1] as! [Any]
        let headingobj = sectionarryobj[0] as? [String:Any]
        var rowheight = 50
        if let heading = headingobj!["key"] as? String
        {
            if(heading == "EMPLOYMENT" || heading == "PASSPORT / CNIC INFORMATION")
            {
                rowheight = 140
            }
        }
        return CGFloat(rowheight)
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let vw = UIView()
        let myLabel = UILabel()
        myLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 40)
        myLabel.center = CGPoint(x: self.view.frame.width/2, y: 20)
        myLabel.textAlignment = .center
       
        let sectionarryobj = tableData[section + 1] as! [Any]
        let headingobj = sectionarryobj[0] as? [String:Any]
        if let heading = headingobj!["key"] as? String
        {
            myLabel.text = heading
        }
        myLabel.textColor = UIColor.white
        myLabel.font = UIFont(name:"GillSans", size: 20.0)
        vw.addSubview(myLabel)
        vw.backgroundColor = hexStringToUIColor(hex: "#251704")
        return vw
    }
    
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
    /*    var cell:UITableViewCell?  = tableView.dequeueReusableCell(withIdentifier: "CELL")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CELL")
        }
 
        let sectionarryobj = tableData[indexPath.section + 1] as! [Any]
        let headingobj = sectionarryobj[indexPath.row + 1] as! [String:Any]
        if let keytoshow = Array(headingobj.keys)[0] as? String
        {
            var labletext = keytoshow + " : "
            if let texttoshow = Array(headingobj.values)[0] as? String
            {
                labletext =  labletext + texttoshow
                
            }
            cell?.textLabel?.text = labletext
        }
       
        cell?.backgroundColor = UIColor.clear
        cell?.textLabel?.textColor = UIColor.white
        return cell!
 
 */
        var cell:UITableViewCell?  = tableView.dequeueReusableCell(withIdentifier: "CELL")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.value1, reuseIdentifier: "CELL")
        }
       
        let sectionarryobj = tableData[indexPath.section + 1] as! [Any]
        let headingobj = sectionarryobj[0] as? [String:Any]
        if let heading = headingobj!["key"] as? String
        {
            if(heading == "EMPLOYMENT" || heading == "PASSPORT / CNIC INFORMATION")
            {
               let celll: ProfilewidgetTableViewCell = tableView.dequeueReusableCell(withIdentifier: "ProfilewidgetTableViewCell", for: indexPath) as! ProfilewidgetTableViewCell
                celll.configureCell(celldata: sectionarryobj)
                return celll
            }
            else
            {
               
                
                let dataobj = sectionarryobj[1] as? [String:Any]
                if let datacertificates = dataobj!["data"] as? [[String:String]]
                {
                    
                    let celll: CrewCertificateTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CrewCertificateTableViewCell", for: indexPath) as! CrewCertificateTableViewCell
                    if(indexPath.row  != 0)
                    {
                    celll.configureCell(celldata: datacertificates[indexPath.row - 1])
                    }
                    else
                    {
                        celll.configureCell(celldata: ["heading":""])
                    }
                    return celll
                }
            }
            
        }
        
        
       // cell.delegate = self
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

