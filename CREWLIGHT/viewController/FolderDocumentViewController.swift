//
//  FolderDocumentViewController.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 26/03/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit
import QuickLook

class FolderDocumentViewController: UIViewController ,UITableViewDataSource,UITableViewDelegate , communicateDelegate , QLPreviewControllerDataSource , QLPreviewControllerDelegate  {
    
    
    @IBOutlet weak var tableview: UITableView!
    
    var menu_id:String = "1"
    var fileurltoopen:URL?
    var folderdocument = [DownloadFolderDocument]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableview.dataSource = self
        tableview.delegate = self
        tableview.layoutMargins = .zero
        tableview.separatorInset = .zero
        
        let notificationName = Notification.Name("filesset")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handlefilesset), name: notificationName, object: nil)
        // Do any additional setup after loading the view.
        renderfiles()
        
        
        let dowloadprogress = Notification.Name("documentdownloadprocess")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handledowloadprogress), name: dowloadprogress, object: nil)
        
        let dowloadcomplete = Notification.Name("documentdownloadcomplete")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadcomplete), name: dowloadcomplete, object: nil)
        
        let dowloadpause = Notification.Name("documentdownloadpause")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadpause), name: dowloadpause, object: nil)
        
        let dowloadstop = Notification.Name("documentdownloadstop")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadstop), name: dowloadstop, object: nil)
        
        let dowloadtriggered = Notification.Name("documentdownloadtriggered")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleddowloadtriggered), name: dowloadtriggered, object: nil)
       
        // Do any additional setup after loading the view.
    }
    
    @objc func handlefilesset(withNotification notification : NSNotification) {
        renderfiles()
    }
    
    @objc func handleddowloadtriggered(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadFolderDocument
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    
    @objc func handledowloadprogress(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadFolderDocument
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    
    
    
    
    @objc func handleddowloadcomplete(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadFolderDocument
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    @objc func handleddowloadpause(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadFolderDocument
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    @objc func handleddowloadstop(withNotification notification : NSNotification) {
        let dowloadobject = notification.object as? DownloadFolderDocument
        updatedocumentcell(dowloadobject:dowloadobject!)
        
    }
    
    func renderfiles()
    {
        folderdocument = []
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let files = appDelegate.fetchdocs(Menu_id: menu_id)
        let currentservices = appDelegate.currentservices
        for object in files {
            
                if(currentservices[object.menu_id!] != nil)
                {
                    folderdocument.append(currentservices[object.menu_id!]!)
                }
                else
                {
                    print(object)
                    let downloadserviceobj = DownloadFolderDocument.init(version: object.version!, uploaddate: object.uploaddate!, menu_parent_id: object.menu_parent_id!, folder_name: object.folder_name!, menu_id: object.menu_id!, filetitle: object.filetitle!, filesize: object.filesize!, filepath: object.filepath!, filename: object.filename!, adm_id: object.adm_id!, add_id: object.add_id!, adc_code: object.adc_code!, adm_ref_id: object.adm_ref_id!)
                    folderdocument.append(downloadserviceobj)
                }
            
        }
        tableview.reloadData()
    }
    
    func updatedocumentcell(dowloadobject:DownloadFolderDocument)
    {
        if(dowloadobject != nil)
        {
            for index in 0..<self.folderdocument.count
            {
                if(self.folderdocument[index].menu_id == dowloadobject.menu_id)
                {
                    self.folderdocument[index] = dowloadobject
                    
                        let indexPath = IndexPath(item: index, section: 0)
                        DispatchQueue.main.async {
                            // self.tableview.reloadRows(at: [indexPath], with: .none)
                            if let cell = self.tableview.cellForRow(at: indexPath) as? DocumentTableViewCell {
                                cell.configureCell(Document: self.folderdocument[index])
                            }
                            
                        }
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folderdocument.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        //        let cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell", for: indexPath) as! DocumentTableViewCell
        //        // cell.delegate = self
        if(folderdocument[indexPath.row].adm_id == "")
        {
            var cell: FolderTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "FolderTableViewCell") as? FolderTableViewCell
            if cell == nil {
                
                tableView.register(UINib(nibName: "FolderTableViewCell", bundle: nil), forCellReuseIdentifier: "FolderTableViewCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "FolderTableViewCell") as? FolderTableViewCell
                
            }
            cell?.delegate = self
            return cell!
        }
        else
        {
        var cell: DocumentTableViewCell? = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell") as? DocumentTableViewCell
        if cell == nil {
            
            tableView.register(UINib(nibName: "DocumentTableViewCell", bundle: nil), forCellReuseIdentifier: "DocumentTableViewCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "DocumentTableViewCell") as? DocumentTableViewCell
            
            
        }
        cell?.delegate = self
        return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if(folderdocument[indexPath.row].adm_id == "")
        {
            if let folderCell = cell as? FolderTableViewCell  {
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                let filescounts = appDelegate.getinnerfilecount(Menu_id: self.folderdocument[indexPath.row].menu_id!)
                print(filescounts)
                self.folderdocument[indexPath.row].filescount = filescounts["files"]!
                self.folderdocument[indexPath.row].folderscount = filescounts["folder"]!
                
                folderCell.configureCell(Document: self.folderdocument[indexPath.row])
                
                
            }
        }
        else
        {
            if let DocumentCell = cell as? DocumentTableViewCell  {
                DocumentCell.configureCell(Document: self.folderdocument[indexPath.row])
            }
        }
    
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(folderdocument[indexPath.row].adm_id == "")
        {
            if let folderdocumentviewcontroller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FolderDocumentViewController") as? FolderDocumentViewController {
                if let navigator = navigationController {
                    
                    folderdocumentviewcontroller.title = folderdocument[indexPath.row].folder_name
                    folderdocumentviewcontroller.menu_id = folderdocument[indexPath.row].menu_id!
                    folderdocumentviewcontroller.navigationItem.backBarButtonItem?.title = ""
                    navigator.pushViewController(folderdocumentviewcontroller, animated: true)
                }
            }
        }
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = UIColor.black
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let selectedCell:UITableViewCell = tableView.cellForRow(at: indexPath)!
        selectedCell.contentView.backgroundColor = hexStringToUIColor(hex: "#251704")
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 80
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tapbtn(sender: Any?, Cell: UITableViewCell?, identifier: String?, Data: Any?) {
        
        
            if(identifier == "viewdocument" )
            {
                let filedownload = Data as? DownloadFolderDocument
                if(filedownload != nil)
                {
                    if(filedownload?.filepath != nil && filedownload?.filepath != "")
                    {
                        
                        self.fileurltoopen = URL(fileURLWithPath: (filedownload?.filepath)!)
                        
                        let viewPDF = QLPreviewController()
                        viewPDF.dataSource = self
                        if let navigator = navigationController {
                            viewPDF.navigationItem.backBarButtonItem?.title = ""
                            navigator.pushViewController(viewPDF, animated: true)
                        }
                        
                        
                    }
                }
            }
            if(identifier == "resetdocument" )
            {
                
                
                let askAlert = UIAlertController(title: "Restart Download", message: "Are you sure you want to restart this download ?", preferredStyle: UIAlertControllerStyle.alert)
                
                askAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                    
                    let documentobj = Data as? DownloadFolderDocument
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    if(appDelegate.networkavalaible)
                    {
                        appDelegate.startnewservice(menu_id: (documentobj?.menu_id)!)
                    }
                    
                }))
                
                askAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                
                present(askAlert, animated: true, completion: nil)
            }
            
            if(identifier == "stopdocument" )
            {
                
                
                let askAlert = UIAlertController(title: "Stop Download", message: "Are you sure you want to stop this download ?", preferredStyle: UIAlertControllerStyle.alert)
                
                askAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                    
                    let documentobj = Data as? DownloadFolderDocument
                    documentobj?.cancelDownload()
                    
                }))
                
                askAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                    print("Handle Cancel Logic here")
                }))
                
                present(askAlert, animated: true, completion: nil)
            }
        
       
    }
    
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        //        let path = Bundle.main.path(forResource: "my", ofType: "pdf")
        //        let url = NSURL.fileURL(withPath: path!)
        return self.fileurltoopen as! QLPreviewItem
    }
    
    func previewControllerWillDismiss(_ controller: QLPreviewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    

   

}
