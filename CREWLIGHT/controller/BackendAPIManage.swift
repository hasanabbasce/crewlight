import Alamofire

class BackendAPIManager: NSObject {
    static let sharedInstance = BackendAPIManager()
    
    var alamoFireManager : Alamofire.SessionManager!
    
    var backgroundCompletionHandler: (() -> Void)? {
        get {
            return alamoFireManager?.backgroundCompletionHandler
        }
        set {
            alamoFireManager?.backgroundCompletionHandler = newValue
        }
    }
    
    fileprivate override init()
    {
        let configuration = URLSessionConfiguration.background(withIdentifier: "com.url.background")
        configuration.timeoutIntervalForRequest = 14400 // seconds
        configuration.timeoutIntervalForResource = 14400
        self.alamoFireManager = Alamofire.SessionManager(configuration: configuration)
    }
}
