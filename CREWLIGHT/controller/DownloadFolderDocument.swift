//
//  DownloadService.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 23/02/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import Foundation
import Alamofire

// Downloads song snippets, and stores in local file.
// Allows cancel, pause, resume download.
class DownloadFolderDocument {
    
    var version : String?
    var uploaddate: String?
    var menu_parent_id: String?
    var folder_name: String?
    var menu_id: String?
    var filetitle: String?
    var filesize: String?
    var filepath: String?
    var filename: String?
    var dowloadeddate: String?
    var adm_id: String?
    var add_id: String?
    var adc_code: String?
    var adm_ref_id: String?
    var filescount: Int = 0
    var folderscount: Int = 0
    var triggerdownload:Bool = false
    
    var deviceid : String = UIDevice.current.identifierForVendor!.uuidString
    var apitoken : String = MD5(string: UIDevice.current.identifierForVendor!.uuidString + "shaheenios")
    
    var downloaded:Bool = false
    var progress : Double = 0
    var ispause : Bool = false
    var idlestate : Bool = false
    
    var request: Alamofire.Request?
    
    var netService = Networkingservice()
    
    init(version:String , uploaddate: String,menu_parent_id: String,folder_name: String,menu_id:String,filetitle:String,filesize:String,filepath:String,filename:String,adm_id:String,add_id:String,adc_code:String,adm_ref_id:String){
        self.version = version
        self.uploaddate = uploaddate
        self.menu_parent_id = menu_parent_id
        self.folder_name = folder_name
        self.menu_id = menu_id
        self.filetitle = filetitle
        self.filesize = filesize
        self.filepath = filepath
        self.filename = filename
        self.adm_id = adm_id
        self.add_id = add_id
        self.adc_code = adc_code
        self.adm_ref_id = adm_ref_id
    }
    
    func startDownload(completion: @escaping ([String:Any?])->()) {
        
        self.idlestate = false
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            
            let documentsURL:NSURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first! as    NSURL
            
            print("***documentURL: ",documentsURL)
            
            let PDF_name : String = self.filename!
            
            let fileURL = documentsURL.appendingPathComponent(PDF_name)
            
            print("***fileURL: ",fileURL ?? "")
            
            return (fileURL!,[.removePreviousFile, .createIntermediateDirectories])
            
        }
        
        
        let escapedfilename = self.filename!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let escapedfoldername = self.adc_code!.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        let fileurltodownload = URL(string: "http://172.16.24.205/efb/?API_TOKEN=" + apitoken + "&DEVICE_ID=" + deviceid + "&category=" + escapedfoldername! + "&filename=" + escapedfilename!)!
        self.request = BackendAPIManager.sharedInstance.alamoFireManager.download(fileurltodownload, to: destination).downloadProgress(closure: { (prog) in
            self.progress = prog.fractionCompleted
           // completion(["Progress":prog.fractionCompleted])
           
            print("Progress: \(prog.fractionCompleted)")
            
            let notificationName = Notification.Name("documentdownloadprocess")
            // Post notification
            NotificationCenter.default.post(name: notificationName, object: self)
            
            // Stop listening notification
            NotificationCenter.default.removeObserver(self, name: notificationName, object: self)
            
        }).response { response in
            
            
            if response.error == nil, let filepathh = response.destinationURL?.path    {
                
                print("Downloaded")                //                print(filepathh)
                self.filepath = filepathh
                self.downloaded = true
                
              //  completion(["Complete":filepathh])
                
                let notificationName = Notification.Name("documentdownloadcomplete")
                // Post notification
                NotificationCenter.default.post(name: notificationName, object: self)
                
                // Stop listening notification
                NotificationCenter.default.removeObserver(self, name: notificationName, object: self)
                
                
                
                
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    let postDate = NSDate().timeIntervalSince1970 as NSNumber
                    self.dowloadeddate = String(describing: postDate)
                appDelegate.updatedoc(menu_id: self.menu_id!, filepath: filepathh, dowloadeddate: self.dowloadeddate! )
                    
                    appDelegate.currentservices.removeValue(forKey: self.menu_id!)
                    let date : Date = Date()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm:ss"
                if(self.adm_ref_id != "")
                {
                    appDelegate.deletedoc(adm_id: self.adm_ref_id!)
                }
                
                
                    let todaysDate = dateFormatter.string(from: date)
                    self.netService.completedownloadfile(ADD_ID: self.add_id, DOWNLOAD_DATE: todaysDate, completion: { (ress) in
                        
                        if(!self.triggerdownload)
                        {
                        appDelegate.nexttodownload()
                        }
                    })
                    
                
            }
            else
            {
                
                
                
            }
            
        }
        
    }
    
    func pauseDownload() {
        self.request?.suspend()
        self.ispause = true
        let notificationName = Notification.Name("documentdownloadpause")
        // Post notification
        NotificationCenter.default.post(name: notificationName, object: self)
        
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: notificationName, object: self)
    }
    
    func resumeDownload() {
        self.request?.resume()
        self.ispause = false
        
    }
    
    func cancelDownload() {
        self.request?.cancel()
        self.ispause = false
        self.progress = 0
        let notificationName = Notification.Name("documentdownloadstop")
        // Post notification
        NotificationCenter.default.post(name: notificationName, object: self)
        
        // Stop listening notification
        NotificationCenter.default.removeObserver(self, name: notificationName, object: self)
    }
}

