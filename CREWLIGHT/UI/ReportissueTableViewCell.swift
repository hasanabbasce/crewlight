//
//  ReportissueTableViewCell.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 12/03/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ReportissueTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var yourquery: UILabel!
    
    @IBOutlet weak var admincomment: UILabel!
    
    
    @IBOutlet weak var admincommentheading: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
