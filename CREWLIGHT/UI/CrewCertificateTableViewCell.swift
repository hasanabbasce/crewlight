//
//  CrewCertificateTableViewCell.swift
//  CREWLIGHT
//
//  Created by Hasan Abbas on 03/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class CrewCertificateTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var certificatelable: UILabel!
    
    @IBOutlet weak var issuedate: UILabel!
    
    @IBOutlet weak var expirydate: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(celldata:  [String:String]){
        if let heading =  celldata["heading"] as? String
        {
            certificatelable.text = "CERTIFICATE"
            certificatelable.textColor = hexStringToUIColor(hex: "#D6B05E")
            issuedate.text = "LAST ATTENDED"
            issuedate.textColor = hexStringToUIColor(hex: "#D6B05E")
            expirydate.text = "EXPIRE DATE"
            expirydate.textColor = hexStringToUIColor(hex: "#D6B05E")
        }
        else
        {
        if let VALID_DESC =  celldata["VALID_DESC"] as? String
        {
            certificatelable.text = VALID_DESC
        }
        if let ISSUE_DATE =  celldata["ISSUE_DATE"] as? String
        {
            issuedate.text =  ISSUE_DATE
        }
        if let EXPIRE_DATE =  celldata["EXPIRE_DATE"] as? String
        {
            expirydate.text =  EXPIRE_DATE
        }
        }
    }
    
}
