//
//  FolderTableViewCell.swift
//  EFB Shaheen Air
//
//  Created by Hasan Abbas on 26/03/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class FolderTableViewCell: UITableViewCell {

    weak var delegate:communicateDelegate?
    var documentobj:DownloadFolderDocument?
    
    @IBOutlet weak var foldername: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
      
      
        // Configure the view for the selected state
    }
    
    func configureCell(Document:  DownloadFolderDocument){
        documentobj = Document
        var counttext:String = Document.folder_name! + " ( "
        counttext = counttext +  "\(documentobj?.folderscount ?? 0)" + " / " + "\(documentobj?.filescount ?? 0)" + " ) "
        foldername.text = counttext
    }
    
}
