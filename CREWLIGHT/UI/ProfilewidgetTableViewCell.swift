//
//  ProfilewidgetTableViewCell.swift
//  CREWLIGHT
//
//  Created by Hasan Abbas on 03/04/2018.
//  Copyright © 2018 Hasan Abbas. All rights reserved.
//

import UIKit

class ProfilewidgetTableViewCell: UITableViewCell {
    
    @IBOutlet weak var leftone: UILabel!
    
    @IBOutlet weak var lefttwo: UILabel!
    
    @IBOutlet weak var leftthree: UILabel!
    
    @IBOutlet weak var leftfour: UILabel!
    
    @IBOutlet weak var rightone: UILabel!
    
    @IBOutlet weak var righttwo: UILabel!
    
    @IBOutlet weak var rightthree: UILabel!
    
    @IBOutlet weak var rightfour: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(celldata:  [Any]){
       
        for i in 1 ..< 9 {
            var texttoshow:String = ""
            var labletoshow:String = ""
            if let headingobj = celldata[i] as? [String:Any]
            {
                if let keytoshow = Array(headingobj.keys)[0] as? String
                {
                    labletoshow = keytoshow
                    var labletext = keytoshow + " : "
                    if let texttoshow = Array(headingobj.values)[0] as? String
                    {
                        labletext =  labletext + texttoshow
                        
                    }
                    texttoshow = labletext
                    
                }
            }
            if(i == 1)
            {
                leftone.text = texttoshow
                leftone.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 2)
            {
                lefttwo.text = texttoshow
                lefttwo.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 3)
            {
                leftthree.text = texttoshow
                leftthree.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 4)
            {
                leftfour.text = texttoshow
                leftfour.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 5)
            {
                rightone.text = texttoshow
                rightone.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 6)
            {
                righttwo.text = texttoshow
                righttwo.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 7)
            {
                rightthree.text = texttoshow
                rightthree.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
            else if(i == 8)
            {
                rightfour.text = texttoshow
                rightfour.halfTextColorChange(fullText: texttoshow, changeText: labletoshow)
            }
        }
        
//        let headingobj = sectionarryobj[indexPath.row + 1] as! [String:Any]
//        if let keytoshow = Array(headingobj.keys)[0] as? String
//        {
//            var labletext = keytoshow + " : "
//            if let texttoshow = Array(headingobj.values)[0] as? String
//            {
//                labletext =  labletext + texttoshow
//
//            }
//            cell?.textLabel?.text = labletext
//        }
    }
    
    
   
    
}
