<?php

if(isset($_REQUEST["category"]) && isset($_REQUEST["filename"]))
{
$folder = "";
if($_REQUEST["category"] == "FRM")
{
$folder = "Forms";
} else if($_REQUEST["category"] == "NOTE")
{
$folder = "Notes";
} else if($_REQUEST["category"] == "MAN")
{
$folder = "Manuals";
}

$file = $folder.'/'.$_REQUEST["filename"];
$filename = $_REQUEST["filename"];
$ext = pathinfo($file, PATHINFO_EXTENSION);

header('Content-type: application/'.$ext);
header('Content-Disposition: inline; filename="' . $filename . '"');
header('Content-Transfer-Encoding: binary');
header('Content-Length: ' . filesize($file));
header('Accept-Ranges: bytes');
@readfile($file);

}

?>